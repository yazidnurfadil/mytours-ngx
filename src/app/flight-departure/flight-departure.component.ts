import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';

library.add(faCoffee);

@Component({
  selector: 'mytours-flight-departure',
  templateUrl: './flight-departure.component.html',
  styleUrls: ['./flight-departure.component.scss']
})


export class FlightDepartureComponent implements OnInit {
  @Output() showFlight = new EventEmitter;
  @Input() currentFlight = 'departure';
  isShowFilter: boolean = false;
  isShowMoreDate: boolean = false;
  constructor(public router: Router) { }
  
  slides = [
    {date: "Wed, 04 May", price: "650000", status: "disabled"},
    {date: "Wed, 04 May", price: "650000", status: "disabled"},
    {date: "Wed, 04 May", price: "650000", status: "available"},
    {date: "Wed, 04 May", price: "650000", status: "available active"},
    {date: "Wed, 04 May", price: "650000", status: "available"},
    {date: "Wed, 04 May", price: "650000", status: "available"},
    {date: "Wed, 04 May", price: "650000", status: "available"},
    {date: "Wed, 04 May", price: "650000", status: "available"},
    {date: "Wed, 04 May", price: "650000", status: "available"},
    {date: "Wed, 04 May", price: "650000", status: "available"},
    {date: "Wed, 04 May", price: "650000", status: "available"},
    {date: "Wed, 04 May", price: "650000", status: "available"},
    {date: "Wed, 04 May", price: "650000", status: "available"},
    {date: "Wed, 04 May", price: "650000", status: "available"},
    {date: "Wed, 04 May", price: "650000", status: "available"},
    {date: "Wed, 04 May", price: "650000", status: "available"},
    {date: "Wed, 04 May", price: "650000", status: "available"}
  ];
  slideConfig = {
                  "slidesToShow": 7,
                  "slidesToScroll": 7,
                  "variableWidth": true,
                  "arrows": false,
                  "infinite": false,
                  // "prevArrow": "<fa-icon [icon]="+'"'+"['fas', 'coffee']"+'"></fa-icon>',
                  // "nextArrow": '<i class="fas fa-angle-right slick-arrow-btn  slick-next-btn">'
                };
  
  addSlide() {
    this.slides.push({date: "Wed, 04 May", price: "650000", status: "available"})
  }
  
  removeSlide() {
    this.slides.length = this.slides.length - 1;
  }
  
  slickInit(e) {
    console.log('slick initialized');
  }
  
  breakpoint(e) {
    console.log('breakpoint');
  }
  
  afterChange(e) {
    console.log('afterChange');
  }
  
  beforeChange(e) {
    console.log('beforeChange');
  }
  ngOnInit() {

  }

  choseReturn() {
    this.isShowMoreDate = false;
    this.showFlight.emit('return');
  }
  choseSelf(e) {
    this.showFlight.emit('departure');
  }
  toggleOtherDate(e) {
    this.isShowMoreDate = !this.isShowMoreDate;
  }
}