import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MytoursFooterComponent } from './mytours-footer.component';

describe('MytoursFooterComponent', () => {
  let component: MytoursFooterComponent;
  let fixture: ComponentFixture<MytoursFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MytoursFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MytoursFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
