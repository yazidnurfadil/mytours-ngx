import { Component, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'mytours-mytours-header',
  templateUrl: './mytours-header.component.html',
  styleUrls: ['./mytours-header.component.scss']
})

export class MytoursHeaderComponent implements OnInit {
  isNavbarWhite : boolean = false;

  constructor(
    private renderer: Renderer2
  ) {}

  ngOnInit() {
    this.renderer.listen(window, "scroll", (e)=> {
      if (e.path[1].scrollY > 80) {
        this.isNavbarWhite = true;
      }else {
        this.isNavbarWhite = false;
      }
    } )
  }


}
