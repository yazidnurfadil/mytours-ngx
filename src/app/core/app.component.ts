import { Component } from '@angular/core';

@Component({
  selector: 'mytours-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mytours';
}
