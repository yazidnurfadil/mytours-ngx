import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MytoursHeaderComponent } from './mytours-header.component';

describe('MytoursHeaderComponent', () => {
  let component: MytoursHeaderComponent;
  let fixture: ComponentFixture<MytoursHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MytoursHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MytoursHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
