import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'mytours-flight-return',
  templateUrl: './flight-return.component.html',
  styleUrls: ['./flight-return.component.scss']
})
export class FlightReturnComponent implements OnInit {
  
  @Output() showFlight = new EventEmitter;
  @Input() currentFlight;
  constructor() { }

  ngOnInit() {
  }

  choseDeparture() {
    this.showFlight.emit('departure');
    // console.log(this.currentFlight);
    
  }
}
