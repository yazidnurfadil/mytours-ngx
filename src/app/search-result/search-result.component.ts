import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'mytours-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {
  currentFlight: string = 'departure';
  isShowSearch: boolean = false;
  isShowDetailPopup: boolean = false;
  @ViewChild('staticModal') staticModal: ModalDirective;

  constructor() { }

  ngOnInit() { }

  showFlight(val: string) {
    this.currentFlight = val;
  }
  showSearch() {
    this.isShowSearch = !this.isShowSearch;
  }
  openModal() { 
    this.staticModal.show();
  }
  
}
