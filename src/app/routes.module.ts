import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FlightDepartureComponent } from './flight-departure/flight-departure.component';
import { SearchResultComponent } from './search-result/search-result.component';

export const routes: Routes = [
  {
    path: '',
    component: SearchResultComponent
  },
  {
    path: 'flight-1',
    component: FlightDepartureComponent
  },
  {
    path: 'flight-2',
    component: SearchResultComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      {
        enableTracing: true, // <-- debugging purposes only
      }
    )
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export class AppRoutingModule { }