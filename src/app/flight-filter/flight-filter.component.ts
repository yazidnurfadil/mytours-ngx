import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'mytours-flight-filter',
  templateUrl: './flight-filter.component.html',
  styleUrls: ['./flight-filter.component.scss']
})
export class FlightFilterComponent implements OnInit {
  @Input() isShowSearch;
  @Output() showSearch = new EventEmitter;
  constructor() { }

  ngOnInit() {
  }

}
