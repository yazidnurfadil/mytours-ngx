import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchForFlightComponent } from './search-for-flight.component';

describe('SearchForFlightComponent', () => {
  let component: SearchForFlightComponent;
  let fixture: ComponentFixture<SearchForFlightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchForFlightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchForFlightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
