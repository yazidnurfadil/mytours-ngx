import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightDepartureComponent } from './flight-departure.component';

describe('FlightDepartureComponent', () => {
  let component: FlightDepartureComponent;
  let fixture: ComponentFixture<FlightDepartureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightDepartureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightDepartureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
