import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'mytours-search-for-flight',
  templateUrl: './search-for-flight.component.html',
  styleUrls: ['./search-for-flight.component.scss']
})
export class SearchForFlightComponent implements OnInit {
  @Input() isShowSearch;
  
  constructor() { }

  ngOnInit() {
  }

}
