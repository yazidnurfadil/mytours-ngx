import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './core/app.component';
import { AppRoutingModule } from './routes.module';
import { MytoursHeaderComponent } from './mytours-header/mytours-header.component';
import { MytoursFooterComponent } from './mytours-footer/mytours-footer.component';
import { FlightReturnComponent } from './flight-return/flight-return.component';
import { FlightDepartureComponent } from './flight-departure/flight-departure.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { FlightFilterComponent } from './flight-filter/flight-filter.component';
import { SearchForFlightComponent } from './search-for-flight/search-for-flight.component';
import { HideUnusedDirective } from './search-result/hide-unused.directive';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCalendar } from '@fortawesome/free-solid-svg-icons';

library.add(faCalendar);

@NgModule({
  declarations: [
    AppComponent,
    MytoursHeaderComponent,
    MytoursFooterComponent,
    FlightReturnComponent,
    FlightDepartureComponent,
    SearchResultComponent,
    FlightFilterComponent,
    SearchForFlightComponent,
    HideUnusedDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PopoverModule.forRoot(),
    ModalModule.forRoot(),
    SlickCarouselModule,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
